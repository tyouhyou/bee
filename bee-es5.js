
/* global Event fn */

function Ee() {
    if (!(this instanceof Ee)) {
        return new Ee();
    }
    this.event_map = new Map();
    this.once_map = new Map();
    this.one_map = new Map();
    this.all_map = new Map();
}

Ee.prototype = {
    /**
     * Subscript event handler for specified event(s). Dupliate subscript is not allowed.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @returns 
     * @memberof Ee
     */
    sub: function (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype => {
            let hds = this.event_map.get(eventtype);
            if (!hds) {
                hds = new Set();
                this.event_map.set(eventtype, hds);
            }
            hds.add(handler);
        });
        
        return this;
    }
    ,
    unsub: function (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype => {
            let hds = this.event_map.get(eventtype);
            if (hds && hds.has(handler)) {
                hds.delete(handler);
            }
        });
        
        return this;
    }
    ,
    once: function (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype =>
            this.once_map.set(eventtype, handler)
        );

        return this;
    }
    ,
    /**
     * When any one of the event-types occurred, the handler will be called and then removed.
     * It means the listening will be stopped and even when another event in the event-types
     * occurred, nothing happens.
     * 
     * If one handler is registered via this method more than one time, only the last one
     * will be kept, all eventttypes registered before will be just removed.
     * 
     * One or more event-types registered here will not influence the same types subscripted
     * via sub(). That means, If one event-type registered by sub() and one(), and both handler
     * will be called when the event occurs.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @memberof Ee
     */
    one: function(eventtypes, handler) {
        if (!handler || !eventtypes) {
            throw new Error("Invalid parameter.");
        }

        this.one_map.set(handler, new Set(fn.stringToArray(eventtypes)));
        return this;
    }
    ,
    /**
     * After all event-types have occurred, the handler will be called and then removed.
     * 
     * If one handler is registered via this method more than one time, only the last one
     * will be kept, all eventttypes registered before will be just removed.
     * 
     * One or more event-types registered here will not influence the same types subscripted
     * via sub(). That means, If one event-type registered by sub() and one(), and both handler
     * will be called when the event occurs.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @memberof Ee
     */
    all: function(eventtypes, handler) {
        if (!handler || !eventtypes) {
            throw new Error("Invalid parameter.");
        }
        this.all_map.set(handler, new Set(fn.stringToArray(eventtypes)));
        return this;
    }
    ,
    pub: function (eventtype, arg, thisarg=null, run=bee_TimeToRun.immediate) {
        let ctx = thisarg ? thisarg : this;

        // call handlers in one_map
        this.one_map.forEach(function(keyvalue) {
            var hd = keyvalue[0];
            var ets = keyvalue[1];
            if(ets.has(eventtype)) {
                this._handle(hd, arg, ctx, run);
                this.one_map.delete(hd);
            }
        }, this);
        
        this.all_map.forEach(function(keyvalue) {
            var hd = keyvalue[0];
            var ets = keyvalue[1];
            if(ets.delete(eventtype) && 0 == ets.size) {
                this._handle(hd, arg, ctx, run);
                this.all_map.delete(hd);
            }
        });

        // call handlers in once_map
        let hds = this.once_map.get(eventtype);
        if (hds) {
            hds.forEach(function(hd) {
                this._handle(hd, arg, ctx, run);
            }, this);
            hds.delete(eventtype);
        }

        // call handlers in event_map
        hds = this.event_map.get(eventtype);
        if (hds) {
            hds.forEach(function(hd) {
                this._handle(hd, arg, ctx, run);
            }, this);
        }
        
        return this;
    }
    ,
    _handle: function(handler, arg, thisarg, timetorun) {
        let hd = handler;
        if (thisarg) {
            hd = handler.bind(thisarg);
        }
    
        switch(timetorun) {
            case bee_TimeToRun.nexttick:
                Promise.resolve().then(() => hd(arg));
                break;
            case bee_TimeToRun.nextturn:
                setTimeout(() => hd(arg), 0);
                break;
            default:
                hd(arg);
                break;
        }
        
        return this;
    }
};

var KEY_EVT_PROXY = "event_proxy";
var KEY_EVT_HANDLER = "event_handler";
var KEY_EVT_LISTENER = "event_listener";

/**
 * 
 * Listener will be called in the order it's registered.
 * 
 * @class BeeE
 */
function BeeE(elem) {
    if (!(this instanceof BeeE)) {
        return new BeeE(elem);
    }
    this._element = elem;
    this._attached = true;
    this.eventMap = new Map();
}

BeeE.prototype = {
    element : function(v) {
        if (arguments.length > 0) {
            this._element = v;
            return this;
        }
        return this._element;
    }
    ,
    attached : function(v) {
        if (arguments.length > 0) {
            this._attached = v;
            return this;
        }
        return this._attached;
    }
    ,
    addEventListener : function(eventtype, handler, useCapture=false, thisarg=null) {
        if (!this.eventMap.has(eventtype)) {
            this.eventMap.set(eventtype, new Map());
        }

        if (this.eventMap.get(eventtype).has(handler)) {
            return;         // duplicate
        }

        var listener = this.makeListener(eventtype, handler, thisarg);
        // duplicate would be overwritten
        this.eventMap.get(eventtype).set(handler, {
            KEY_EVT_LISTENER: listener,
        });

        this._element.addEventListener(eventtype, listener, useCapture);
        
        return this;
    }
    ,
    removeEventListener : function(eventtype, handler) {
        if (this.eventMap.has(eventtype)) {
            if (handler) {
                this._element.removeEventListener(eventtype, this.eventMap.get(eventtype).get(handler).KEY_EVT_PROXY);
                this.eventMap.get(eventtype).delete(handler);
            } else {
                this.eventMap.get(eventtype).forEach((obj, listener) => {
                    this._element.removeEventListener(eventtype, obj.KEY_EVT_PROXY);
                    this.eventMap.get(eventtype).delete(listener);
                });
            }
            if (this.eventMap.get(eventtype).size === 0) {
                this.eventMap.delete(eventtype);
            }
        }
        return this;
    }
    ,
    removeAllListeners : function() {
        this.eventMap.forEach((obj, eventtype) => {
            obj.forEach((o, listener) => {
                this._element.removeEventListener(eventtype, o.KEY_EVT_PROXY);
                obj.delete(listener);
            });
        });
        return this;
    }
    ,
    dispatchEvent : function(eventtype, data=undefined) {
        var event = new Event(eventtype, data);
        this._element.dispatchEvent(event);
        return this;
    }
    ,
    getListener : function(eventtype, handler) {
        return !this.eventMap.has(eventtype)
             ? null
             : !this.eventMap.get(eventtype).has(handler)
             ? this.eventMap.get(eventtype).get(handler).KEY_EVT_LISTENER
             : null
             ;
    }
    ,
    makeListener : function(eventtype, handler, thisarg) {
        return function(event) {
            if (!this.attached()) {
                return;
            }
            
            this.eventMap.get(eventtype).set(KEY_EVT_HANDLER, handler);
            var proxy = this.eventMap.get(eventtype).get(KEY_EVT_PROXY);
            if (proxy && typeof proxy === "function") {
                if (thisarg && typeof thisarg === "object") {
                    proxy = proxy.bind(thisarg);
                }
                proxy(handler, event);
            } else {
                if (thisarg && typeof thisarg === "object") {
                    handler = handler.bind(thisarg);
                }
                handler(event);
            }
            // TIMEs? async?
            // web worker?
        }.bind(this);
    }
    ,
    proxy : function(eventtypes, func) {
        var evtList = fn.stringToArray(eventtypes);
        evtList.forEach(eventtype => {
            var evt = this.eventMap.get(eventtype);
            if (evt) {
                evt.set(KEY_EVT_PROXY, func);
            }
        });
        return this;
    }
};

function Bee(elements) {
    /**
     * Creates an instance of Bee.
     * @param {any} [elements=null] A string or string array of selector(s) or an elements array.
     * @memberof Bee
     */
    if (!elements) elements = null;
    this.elementMap = new Map();        // containing element-event map
    this.bind(elements);
}

Bee.prototype = {
    bind : function(elements) {
        if (elements) {
            fn.flattenElements(elements).forEach(element => {
                if (!this.elementMap.has(element)) {
                    this.elementMap.set(element, null);
                }
            });
        }
        return this;
    }
    ,
    unbind : function(elements) {
        if (elements) {
            fn.flattenElements(elements).forEach(element => {
                if (this.elementMap.has(element)) {
                    var beee = this.elementMap.get(element);
                    beee.removeAllListeners();
                    this.elementMap.delete(beee);
                }
            });
        }
        return this;
    }
    ,
    // TODO: add element as the first argument?
    /**
     * Add event handler to element(s)
     * 
     * @param {*} eventtypes                Event type names. It can be an array or a string.
     *                                      In case string, space character is used to delimit the types.
     * @param {Function} handler            Event handler.
     * @param {Boolean} [useCapture=null]   Indicating using capture or bubbling when event fired.
     * @param {*} [thisarg=null]            Binding "this" object to the event handler. 
     *                                      If null, the "element" should be "this" by default.
     * @memberof Bee
     */
    on : function(eventtypes, handler, useCapture=false, thisarg=null) {
        var evtList = fn.stringToArray(eventtypes);
        this.elementMap.forEach((beee, element) => {
            evtList.forEach(eventtype => {
                if (!beee) {
                    this.elementMap.set(element, new BeeE(element));
                }
                this.elementMap.get(element).addEventListener(eventtype, handler, useCapture, thisarg);
            });
        });
        return this;
    }
    ,
    /**
     * Remove event handler from element(s) 
     *
     * @param {any} eventtypes 
     * @param {any} handler 
     * @returns 
     * @memberof Bee
     */
    off : function(eventtypes, handler) {
        var evtList = fn.stringToArray(eventtypes);
        this.elementMap.forEach((beee, element) => {
            evtList.forEach(eventtype => {
                if (beee) {
                    beee.removeEventListener(eventtype, handler);
                    if (beee.size === 0) {
                        this.elementMap.delete(element);
                    }
                }
            });
        });
        return this;
    }
    ,
    /**
     * Require proxy to attach handler so it can react to events.
     * 
     * @param {any} [elements=null] Elements to attach. Refer to on() for detail information.
     * @memberof Bee
     */
    attach : function() {
        this.elementMap.forEach((beee) => {
            beee.attached(true);
        });
        return this;
    }
    ,
    /**
     * Require proxy to detach handler from responsing events. So even when event occurred,
     * the handler would not react to it.
     * 
     * @param {any} [elements=null] Elements to detach. Refer to on() for detail information.
     * @memberof Bee
     */
    detach : function() {
        this.elementMap.forEach((beee) => {
            beee.attached(false);
        });
        return this;
    }
    ,
    /***
     * @param {any} func Proxy of the registered event handler.
     *                   The contract of this function is func(handler, event).
     *                   The proxy can be removed by setting argument func to null or undefined.
     * */
    proxy : function(eventtypes, func) {
        if (func && typeof func !== "function") {
            return;
        }
        this.elementMap.forEach((beee) => {
            beee.proxy(eventtypes, func);
        });
        return this;
    }
    ,
    /**
     * Fire an event to the element(s)
     * 
     * @param {any} eventtype 
     * @param {any} data 
     * @param {any} [elements=null] Elements to which dispatch event. Refer to on() for detail information.
     * @memberof Bee
     */
    dispatch : function(eventtype, data) {
        this.elementMap.forEach((beee) => {
            beee.dispatchEvent(eventtype, data);
        });
        return this;
    }
};

var bee_TimeToRun = Object.freeze({
    immediate: "immediate",
    nexttick : "nexttick",
    nextturn : "nextturn"
});

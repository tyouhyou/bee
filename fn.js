export { fn };

/* global Element, 
          HTMLElement,
 */
 
var fn = {
    isObject : function(obj) {
        return obj !== null && typeof obj === "object";
    }
    ,
    isCallable : function (fn) {
      return typeof fn === 'function' || Object.prototype.toString.call(fn) === '[object Function]';
    }
    ,
    isElement : function(obj) {
        return !obj
                ? false
                : (Element && typeof Element === "object")
                ? obj instanceof Element
                : (HTMLElement && typeof HTMLElement === "object")
                ? obj instanceof HTMLElement
                : (typeof obj === "object" && obj.nodeType === 1 && typeof obj.nodeName === "string")
                ;
    }
    ,
    /* global Node */
    isNode: function(obj) {
        return typeof Node === "object"
                ? obj instanceof Node
                : obj && typeof obj === "object" && typeof obj.nodeType === "number" && typeof obj.nodeName === "string"
                ;
    }
    ,
    isHtml: function(value) {
        var regexx = /^\s*<.*>\s*$/,
            regex1 = /\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>]*)\s*>(.*)<\/([A-Za-z][A-Za-z0-9]+)\s*>\s*/g,
            regex2 = /\s*<([A-Za-z][A-Za-z0-9]+)\s*([^<>\/]*)\s*\/{0,1}>\s*/g
            ;
        return regexx.test(value) && (regex1.test(value) | regex2.test(value));
    }
    ,
    isString : function(s) {
        return typeof s === "string" || s instanceof String;
    }
    ,
    isArrayOrLike : function(a) {
        return !a
                ? false
                : a instanceof Array
                ? true
                : typeof(a) == "object" && a.hasOwnProperty("length") && a.length >= 0 && a.length < Number.MAX_SAFE_INTEGER
                ;
    }
    ,
    /**
     * @description The objects which can apply forEach() to, but not enumerable (which means an object here.)
     *              Check if the target object can be used in for() loop.
     */
    isIterable : function(obj) {
        let rst = false,
            strType = Object.prototype.toString.call( obj );
        
        // TODO: add set, map, iterator?
        if (fn.isArrayOrLike(obj)
        ||  /^\[object (HTMLCollection|NodeList)\]$/.test(strType))
        {
            rst = true;
        }
            
        return rst;
    }
    ,
    isUndefine : function(obj) {
        return typeof obj === 'undefined';
    }
    ,
    isDate : function(dat) {
        return Object.prototype.toString.call(dat) === '[object Date]';
    }
    ,
    isRegexp : function(reg) {
        return Object.prototype.toString.call(reg) === '[object RegExp]';
    }
    ,
    /***
     * Parse words in a string delimited by space into word arrays.
     * */
    stringToArray : function(s) {
        let arr = s;
        if (fn.isString(s)) {
            arr = s.split(" ");
            arr = arr.filter(a => {
                    a = a.trim();
                    return !!a;
                });
        }
        return arr;
    }
    ,
    flattenArrayOrLike : function(arr) {
        let a = [];
        if (fn.isArrayOrLike(arr))
        for (let i = 0; i < arr.length; i++) {
            if (fn.isArrayOrLike(arr[i])) {
                a = a.concat(a, arr[i]);
            } else {
                a.push(arr[i]);
            }
        }
        return a;
    }
    ,
    flattenElements : function(elements, arr) {
        if (!window || !window.document) {
            throw new Error("Illegal operation. The context seems not a browser window object.");
        }

        if (arr && !fn.isArrayOrLike(arr)) {
            throw new Error("Invalid argment. The second argument should be an array.");
        }
        
        let elelist = arr || [];

        if (fn.isString(elements)) {
            elelist = window.document.body.querySelectorAll(elements);
        } else if (fn.isArrayOrLike(elements)) {
            fn.flattenArrayOrLike(elements).forEach( el => {
                if (fn.isString(el)) {
                    elelist = elelist.concat(elelist, window.document.body.querySelectorAll(el));
                } else if (fn.isElement(el)) {
                    elelist.push(el);
                } else {
                    throw new Error("Invalid argument. Selectors or HtmlElement objects are expected.");
                }
            });
        } else {
            throw new Error("Invalid argument. A string or an array (or -like) is expected");
        }

        return elelist;
    }
};


/***
 * Polyfill for es6 Map object. Not all methods will be implemented.
 * Just those methods we will use.
 * 
 * Since this polyfill is just for using with es5, iterator would not be honored.
 * */
(function(context) {
    if (!!context.Map) return;
    
    var Map = function(array) {
        if (!(this instanceof Map)) {
            return new Map(array);
        }
        
        var check = true;
        if (!Array.isArray()) {
            check = false;
        }
        array.forEach(function(keyvalue){
            if (!Array.isArray(keyvalue) || keyvalue.length != 2) {
                check = false;
                return;
            }
        }, this);
        if (!check) {
            throw new Error("The argument sent to Map constructor should be a key-value pair array.");
        }
        
        this.__keyvaluepairs__ = array || new Array();
        this.size = 0;
    };
    
    Map.prototype = {
        _idxKey: 0,
        _idxValue: 1,
        /***
         * check if the specified key has been set. 
         * out_obj_for_idx is not presented in the standard IF.
         * It is here to keep the key location, and just for using within the Map object.
         * */
        has: function(key, out_obj_for_idx) {
            var ret = false;
            // since Nan cannot be handled correctly by indexOf(), we iterate via forEach.
            this.__keyvaluepairs__.forEach(function(keyvalue, index){
                if ((typeof(key) === "number" && isNaN(key) && 
                    typeof(keyvalue[this._idxKey]) === "number" && isNaN(keyvalue[this._idxKey]))  ||
                    key === keyvalue[0])
                {
                    ret = true;
                    if (!!out_obj_for_idx && (out_obj_for_idx instanceof Object)) {
                        out_obj_for_idx.index = index;
                    }
                    return;
                }
            }, this);
            return ret;
        }
        ,
        set: function(key, value) {
            if (this.has(key)) {
                this.__keyvaluepairs__[key][this._idxValue] = value;
            } else {
                this.__keyvaluepairs__.push([key, value]);
                this.size ++;
            }
            return this;
        }
        ,
        get: function(key) {
            var ret;
            if (this.has(key)) {
                ret = this.__keyvaluepairs__[key];
            }
            return ret;
        }
        ,
        clear: function() {
            this.__keyvaluepairs__.length = 0;
            this.size = 0;
            return undefined;
        }
        ,
        delete: function(key) {
            var ret = false,
                idx = {};
            if (this.has(key, idx)) {
                this.__keyvaluepairs__.splice(idx.index, 1);
                this.size --;
                ret = true;
            }
            return ret;
        }
        ,
        forEach: function(callback, thisarg){
            var me = this;
            thisarg = thisarg || this;
            this.__keyvaluepairs__.forEach(function(keyvalue) {
                callback(keyvalue[me._idxValue], keyvalue[me._idxKey], me).bind(thisarg);
            }, thisarg);
        }
    };
    
    context.Map = Map;
})(window || global);

(function(context){
    if (!!context.Set) return;
    
    var Set = function(array) {
        if (!(this instanceof Set)) {
            return new Set(array);
        }
        
        this.size = 0;
        this.__data__ = [];
    };
    
    Set.prototype = {
        has: function(value, out_obj_for_idx) {
            var ret = false;
            if (typeof(value) === "number" && isNaN(value)) {
                this.__data__.forEach(function(val, index) {
                    if (typeof(value) === "number" && isNaN(val)) {
                        ret = true;
                        out_obj_for_idx.idx = index;
                    }
                });
            } else if ((out_obj_for_idx.idx = this.__data__.indexOf(value, 0)) >= 0) {
                ret = true;
            }
            return ret;
        }
        ,
        add: function(value) {
            if (!this.has(value)) {
                this.__data__.push(value);
                this.size ++;
            }
            return this;
        }
        ,
        clear: function() {
            this.__data__.length = 0;
            this.size = 0;
            return undefined;
        }
        ,
        delete: function(value) {
            var ret = false,
                outIdx = {};
            if (this.has(value, outIdx)) {
                this.__data__.splice(outIdx.idx, 1);
                this.size --;
                ret = true;
            }
            return ret;
        }
        ,
        forEach: function(callback, thisarg) {
            var me = this;
            thisarg = thisarg || this;
            this.__data__.forEach(function(value) {
                callback(value, value, me).bind(thisarg);
            }, thisarg);
        }
    };
    
    context.Set = Set;
})(Set, window || global);

export { Bee , Ee };

import { fn } from "./fn.js";

// TODO: add ->
//       next-tick (to call at next tick), 
//       doEvent (to call after all current waiting events done), 
//       background-worker (on other process)

/* global Event */

/**
 * When event is fired, handlers are called in the following order ->
 * 1. handler(s) subscribed via one(). And then remove the handler(s) from subscription.
 * 2. handler(s) subscribed via all(). If handler was called, then remove it from subscription.
 * 3. handler(s) subscribed via once(). And then remove the handler(s) from subscription.
 * 4. handler(s) subscribed via sub().
 * 
 * @export
 * @class Ee
 */
class Ee {
    constructor() {
        this.event_map = new Map();
        this.once_map = new Map();
        this.one_map = new Map();
        this.all_map = new Map();
    }

    /**
     * Subscript event handler for specified event(s). Dupliate subscript is not allowed.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @returns 
     * @memberof Ee
     */
    sub (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype => {
            let hds = this.event_map.get(eventtype);
            if (!hds) {
                hds = new Set();
                this.event_map.set(eventtype, hds);
            }
            hds.add(handler);
        });
        
        return this;
    }

    unsub (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype => {
            let hds = this.event_map.get(eventtype);
            if (hds && hds.has(handler)) {
                hds.delete(handler);
            }
        });
        
        return this;
    }
    
    once (eventtypes, handler) {
        fn.stringToArray(eventtypes).forEach(eventtype =>
            this.once_map.set(eventtype, handler)
        );

        return this;
    }
    
    /**
     * When any one of the event-types occurred, the handler will be called and then removed.
     * It means the listening will be stopped and even when another event in the event-types
     * occurred, nothing happens.
     * 
     * If one handler is registered via this method more than one time, only the last one
     * will be kept, all eventttypes registered before will be just removed.
     * 
     * One or more event-types registered here will not influence the same types subscripted
     * via sub(). That means, If one event-type registered by sub() and one(), and both handler
     * will be called when the event occurs.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @memberof Ee
     */
    one(eventtypes, handler) {
        if (!handler || !eventtypes) {
            throw new Error("Invalid parameter.");
        }

        this.one_map.set(handler, new Set(fn.stringToArray(eventtypes)));
        return this;
    }

    /**
     * After all event-types have occurred, the handler will be called and then removed.
     * 
     * If one handler is registered via this method more than one time, only the last one
     * will be kept, all eventttypes registered before will be just removed.
     * 
     * One or more event-types registered here will not influence the same types subscripted
     * via sub(). That means, If one event-type registered by sub() and one(), and both handler
     * will be called when the event occurs.
     * 
     * @param {any} eventtypes 
     * @param {any} handler 
     * @memberof Ee
     */
    all(eventtypes, handler) {
        if (!handler || !eventtypes) {
            throw new Error("Invalid parameter.");
        }
        this.all_map.set(handler, new Set(fn.stringToArray(eventtypes)));
        return this;
    }

    pub (eventtype, arg, thisarg=null, run=bee_TimeToRun.immediate) {
        let ctx = thisarg ? thisarg : this;

        // call handlers in one_map
        for(let [hd, ets] of this.one_map) {
            if(ets.has(eventtype)) {
                this._handle(hd, arg, ctx, run);
                this.one_map.delete(hd);
            }
        }

        // call handlers in all_map
        for(let [hd, ets] of this.all_map) {
            if(ets.delete(eventtype) && 0 === ets.size) {
                this._handle(hd, arg, ctx, run);
                this.all_map.delete(hd);
            }
        }

        // call handlers in once_map
        let hds = this.once_map.get(eventtype);
        if (hds) {
            for (let hd of hds) {
                this._handle(hd, arg, ctx, run);
            }
            hds.delete(eventtype);
        }

        // call handlers in event_map
        hds = this.event_map.get(eventtype);
        if (hds) {
            for (let hd of hds) {
                this._handle(hd, arg, ctx, run);
            }
        }
        
        return this;
    }
    
    _handle(handler, arg, thisarg, timetorun) {
        let hd = handler;
        if (thisarg) {
            hd = handler.bind(thisarg);
        }
    
        switch(timetorun) {
            case bee_TimeToRun.nexttick:
                Promise.resolve().then(() => hd(arg));
                break;
            case bee_TimeToRun.nextturn:
                setTimeout(() => hd(arg), 0);
                break;
            default:
                hd(arg);
                break;
        }
        
        return this;
    }
}

const KEY_EVT_PROXY = Symbol("event_proxy");
const KEY_EVT_HANDLER = Symbol("event_handler");
const KEY_EVT_LISTENER = Symbol("event_listener");

/**
 * 
 * Listener will be called in the order it's registered.
 * 
 * @class BeeE
 */
class BeeE {
    constructor(elem) {
        this.element = elem;
        this.eventMap = new Map();
        this._attached = true;
    }

    get element() {
        return this._element;
    }
    
    set element(v) {
        this._element = v;
    }

    get attached() {
        return this._attached;
    }
    set attached(v) {
        this._attached = v;
    }

    addEventListener(eventtype, handler, useCapture=false, thisarg=null) {
        if (!this.eventMap.has(eventtype)) {
            this.eventMap.set(eventtype, new Map());
        }

        if (this.eventMap.get(eventtype).has(handler)) {
            return;         // duplicate
        }

        let listener = this.makeListener(eventtype, handler, thisarg);
        // duplicate would be overwritten
        this.eventMap.get(eventtype).set(handler, {
            KEY_EVT_LISTENER: listener,
        });

        this._element.addEventListener(eventtype, listener, useCapture);
        
        return this;
    }
    
    removeEventListener(eventtype, handler) {
        if (this.eventMap.has(eventtype)) {
            if (handler) {
                this._element.removeEventListener(eventtype, this.eventMap.get(eventtype).get(handler).KEY_EVT_PROXY);
                this.eventMap.get(eventtype).delete(handler);
            } else {
                this.eventMap.get(eventtype).forEach((obj, listener) => {
                    this._element.removeEventListener(eventtype, obj.KEY_EVT_PROXY);
                    this.eventMap.get(eventtype).delete(listener);
                });
            }
            if (this.eventMap.get(eventtype).size === 0) {
                this.eventMap.delete(eventtype);
            }
        }
        return this;
    }
    
    removeAllListeners() {
        this.eventMap.forEach((obj, eventtype) => {
            obj.forEach((o, listener) => {
                this._element.removeEventListener(eventtype, o.KEY_EVT_PROXY);
                obj.delete(listener);
            });
        });
        return this;
    }

    dispatchEvent(eventtype, data=undefined) {
        let event = new Event(eventtype, data);
        this._element.dispatchEvent(event);
        return this;
    }
    
    getListener(eventtype, handler) {
        return !this.eventMap.has(eventtype)
             ? null
             : !this.eventMap.get(eventtype).has(handler)
             ? this.eventMap.get(eventtype).get(handler).KEY_EVT_LISTENER
             : null
             ;
    }

    makeListener(eventtype, handler, thisarg) {
        return function(event) {
            if (!this.attached) {
                return;
            }
            
            this.eventMap.get(eventtype).set(KEY_EVT_HANDLER, handler);
            let proxy = this.eventMap.get(eventtype).get(KEY_EVT_PROXY);
            if (proxy && typeof proxy === "function") {
                if (thisarg && typeof thisarg === "object") {
                    proxy = proxy.bind(thisarg);
                }
                proxy(handler, event);
            } else {
                if (thisarg && typeof thisarg === "object") {
                    handler = handler.bind(thisarg);
                }
                handler(event);
            }
            // TIMEs? async?
            // web worker?
        }.bind(this);
    }
    
    proxy(eventtypes, func) {
        let evtList = fn.stringToArray(eventtypes);
        evtList.forEach(eventtype => {
            let evt = this.eventMap.get(eventtype);
            if (evt) {
                evt.set(KEY_EVT_PROXY, func);
            }
        });
        return this;
    }
}

class Bee {
    /**
     * Creates an instance of Bee.
     * @param {any} [elements=null] A string or string array of selector(s) or an elements array.
     * @memberof Bee
     */
    constructor(elements=null) {
        this.elementMap = new Map();        // containing element-event map
        this.bind(elements);
    }

    bind(elements) {
        if (elements) {
            fn.flattenElements(elements).forEach(element => {
                if (!this.elementMap.has(element)) {
                    this.elementMap.set(element, null);
                }
            });
        }
        return this;
    }
    
    unbind(elements) {
        if (elements) {
            fn.flattenElements(elements).forEach(element => {
                if (this.elementMap.has(element)) {
                    let beee = this.elementMap.get(element);
                    beee.removeAllListeners();
                    this.elementMap.delete(beee);
                }
            });
        }
        return this;
    }

    // TODO: add element as the first argument?
    /**
     * Add event handler to element(s)
     * 
     * @param {*} eventtypes                Event type names. It can be an array or a string.
     *                                      In case string, space character is used to delimit the types.
     * @param {Function} handler            Event handler.
     * @param {Boolean} [useCapture=null]   Indicating using capture or bubbling when event fired.
     * @param {*} [thisarg=null]            Binding "this" object to the event handler. 
     *                                      If null, the "element" should be "this" by default.
     * @memberof Bee
     */
    on(eventtypes, handler, useCapture=false, thisarg=null) {
        let evtList = fn.stringToArray(eventtypes);
        this.elementMap.forEach((beee, element) => {
            evtList.forEach(eventtype => {
                if (!beee) {
                    this.elementMap.set(element, new BeeE(element));
                }
                this.elementMap.get(element).addEventListener(eventtype, handler, useCapture, thisarg);
            });
        });
        return this;
    }

    /**
     * Remove event handler from element(s) 
     *
     * @param {any} eventtypes 
     * @param {any} handler 
     * @returns 
     * @memberof Bee
     */
    off(eventtypes, handler) {
        let evtList = fn.stringToArray(eventtypes);
        this.elementMap.forEach((beee, element) => {
            evtList.forEach(eventtype => {
                if (beee) {
                    beee.removeEventListener(eventtype, handler);
                    if (beee.size === 0) {
                        this.elementMap.delete(element);
                    }
                }
            });
        });
        return this;
    }

    /**
     * Require proxy to attach handler so it can react to events.
     * 
     * @param {any} [elements=null] Elements to attach. Refer to on() for detail information.
     * @memberof Bee
     */
    attach() {
        this.elementMap.forEach((beee) => {
            beee.attached = true;
        });
        return this;
    }

    /**
     * Require proxy to detach handler from responsing events. So even when event occurred,
     * the handler would not react to it.
     * 
     * @param {any} [elements=null] Elements to detach. Refer to on() for detail information.
     * @memberof Bee
     */
    detach() {
        this.elementMap.forEach((beee) => {
            beee.attached = false;
        });
        return this;
    }
    
    /***
     * @param {any} func Proxy of the registered event handler.
     *                   The contract of this function is func(handler, event).
     *                   The proxy can be removed by setting argument func to null or undefined.
     * */
    proxy(eventtypes, func) {
        if (func && typeof func !== "function") {
            return;
        }
        this.elementMap.forEach((beee) => {
            beee.proxy(eventtypes, func);
        });
        return this;
    }

    /**
     * Fire an event to the element(s)
     * 
     * @param {any} eventtype 
     * @param {any} data 
     * @param {any} [elements=null] Elements to which dispatch event. Refer to on() for detail information.
     * @memberof Bee
     */
    dispatch (eventtype, data) {
        this.elementMap.forEach((beee) => {
            beee.dispatchEvent(eventtype, data);
        });
        return this;
    }

    /**
     * TODO: element dispatch async?
     * 
     * @param {any} eventtype 
     * @param {any} data 
     * @param {any} thisarg 
     * @param {any} async 
     * @memberof Bee
     */
    // broadcast (eventtype, data, thisarg, async) {
    //     super.pub(eventtype, data, thisarg, async ? bee_TimeToRun.nextturn : null); // TODO: ? need pub and dispatch at the same time?
    //     this.dispatch(eventtype, data);
    //     return this;
    // }
}

let bee_TimeToRun = Object.freeze({
    immediate: Symbol("immediate"),
    nexttick : Symbol("nexttick"),
    nextturn : Symbol("nextturn"),
});
